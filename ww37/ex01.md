# networking-basics

https://eal-itt.gitlab.io/20a-itt1-networking/weekly-plans/weekly_ww01


Mehrang’s notes:
[Hub, Switch, Routers](https://www.youtube.com/watch?v=1z0ULvg_pW8&ab_channel=PowerCertAnimatedVideos)
which explains really well
[Modem vs Router](https://www.youtube.com/watch?v=Mad4kQ5835Y&ab_channel=PowerCertAnimatedVideos)
Explains differences between Modem & Router
[Firewall](https://www.youtube.com/watch?v=kDEX1HXybrU&ab_channel=PowerCertAnimatedVideos)
Explains how firewalls works
[SSL, TLS, HTTP, HTTPS](https://www.youtube.com/watch?v=hExRDVZHhig&ab_channel=PowerCertAnimatedVideos)
Explaining difference between those
[Subnetting](https://www.youtube.com/watch?v=nFYilGQ-p-8&ab_channel=CarlOliver) 
Explains how to manually figure subnetting out
[OSI model](https://www.youtube.com/watch?v=y9PG-_ZNbWg&ab_channel=NetworkDirection)
Not an amazing explanation nor worse one
